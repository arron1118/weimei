drop table if exists wx_member;
create table wx_member(
  uid int(11) not null auto_increment primary key comment '用户ID',
  username varchar(32) not null default '' comment '用户昵称',
  openid varchar(32) not null default '' comment '微信openid',
  avatar varchar(255) not null default '' comment '头像',
  phone varchar(15) not null default '' comment '联系电话',
  qrcode varchar(255) not null default '' comment '微信二维码',
  proverb varchar(150) not null default '' comment '格言',
  position varchar(32) not null default '' comment '职位',
  city varchar(32) not null default '' comment '城市',
  createtime varchar(11) not null default '' comment '创建时间',
  vip_expire varchar(11) not null default '' comment 'VIP过期时间',
  integral smallint(6) not null default '0' comment '积分',
  signnum smallint(6) not null default '0' comment '签到次数',
  last_sign_time varchar(11) not null default '' comment '最后签到时间',
  sign_order smallint(6) not null default '0' comment '签到排名'
)engine=innodb default charset=utf8 comment='用户表';

drop table if exists wx_deposit;
create table wx_deposit(
  id int(11) not null auto_increment primary key comment 'ID',
  uid int(11) not null default '0' comment '用户ID',
  amount decimal(10,2) not null default '0.00' comment '充值金额',
  wxorderid varchar(32) not null default '' comment '微信订单号',
  createtime varchar(11) not null default '' comment '创建时间'
)engine=innodb default charset=utf8 comment='充值记录表';

drop table if exists wx_integral_log;
create table wx_integral_log(
  id int(11) not null auto_increment primary key comment 'ID',
  uid int(11) not null default '0' comment '用户ID',
  opration enum('ADD', 'MINUS') not null default 'ADD' comment '操作 ADD 增加 MINUS 减少',
  integral smallint(6) not null default '0' comment '积分',
  type tinyint(1) not null default '0' comment '类型 0 注册奖励 1 登录奖励 2 邀请奖励 3 兑换VIP',
  remark varchar(255) not null default '' comment '说明',
  createtime varchar(11) not null default '' comment '创建时间'
)engine=myisam default charset=utf8 comment='积分记录表';

drop table if exists wx_article_category;
create table wx_article_category(
  id smallint(6) not null auto_increment primary key comment 'ID',
  catname varchar(32) not null default '' comment '分类名称',
  pid smallint(6) not null default '0' comment '父ID'
)engine=myisam default charset=utf8 comment='文章分类表';

drop table if exists wx_article;
create table wx_article(
  id int(11) not null auto_increment primary key comment 'ID',
  title varchar(120) not null default '' comment '标题',
  cat_id smallint(6) not null default '0' comment '分类ID',
  decription varchar(255) not null default '' comment '描述',
  pic varchar(120) not null default '' comment '文章图片',
  content text not null comment '内容',
  resource varchar(120) not null default '' comment '文章来源',
  is_issue tinyint(1) not null default '0' comment '是否发布 0 未发布 1 已发布',
  createtime varchar(11) not null default '' comment '创建时间',
  viewnum smallint(6) not null default '0' comment '查看数',
  transnum smallint(6) not null default '0' comment '转发数'
)engine=myisam default charset=utf8 comment='文章表';

drop table if exists wx_goods_category;
create table wx_goods_category(
  id smallint(6) not null auto_increment primary key comment 'ID',
  catname varchar(32) not null default '' comment '分类名称',
  pid smallint(6) not null default '0' comment '父ID'
)engine=myisam default charset=utf8 comment='商品分类表';

drop table if exists wx_goods;
create table wx_goods(
  id int(11) not null auto_increment primary key comment 'ID',
  title varchar(120) not null default '' comment '标题',
  cat_id smallint(6) not null default '0' comment '分类ID',
  description varchar(255) not null default '' comment '描述',
  price decimal(10,2) not null default '0.00' comment '价格',
  pv_price decimal(10,2) not null default '0.00' comment 'PV价格',
  pic varchar(255) not null default '' comment '商品图片',
  detail text not null comment '商品描述',
  is_issue tinyint(1) not null default '0' comment '是否发布 0 未发布 1 已发布',
  createtime varchar(11) not null default '' comment '创建时间',
  viewnum smallint(6) not null default '0' comment '查看数',
  buynum smallint(6) not null default '0' comment '购买数'
)engine=innodb default charset=utf8 comment='商品表';

drop table if exists wx_poster_catogry;
create table wx_poster_catogry(
  id smallint(6) not null auto_increment primary key comment 'ID',
  catname varchar(32) not null default '' comment '分类名称',
  pid smallint(6) not null default '0' comment '父ID'
)engine=myisam default charset=utf8 comment='海报分类表';

drop table if exists wx_poster;
create table wx_poster(
  id int(11) not null auto_increment primary key comment 'ID',
  title varchar(120) not null default '' comment '标题',
  cat_id smallint(6) not null default '0' comment '分类ID',
  pic varchar(255) not null default '' comment '海报图片',
  is_issue tinyint(1) not null default '0' comment '是否发布 0 未发布 1 已发布',
  createtime varchar(11) not null default '' comment '创建时间',
  usenum smallint(6) not null default '0' comment '使用数'
)engine=myisam default charset=utf8 comment='海报表';

drop table if exists wx_orders;
create table wx_orders(
  id int(11) not null auto_increment primary key comment 'ID',
  oid int(11) not null default '0' comment '订单ID',
  wxorderid varchar(32) not null default '' comment '微信订单号',
  master_uid int(11) not null default '0' comment '店主UID',
  buy_uid int(11) not null default '0' comment '买家UID',
  goods_id int(11) not null default '0' comment '商品ID',
  num smallint(6) not null default '1' comment '数量',
  createtime varchar(11) not null default '' comment '创建时间',
  phone varchar(15) not null default '' comment '联系电话',
  message varchar(255) not null default '' comment '留言'
)engine=innodb default charset=utf8 comment='我的订单表';

drop table if exists wx_customer;
create table wx_customer(
  id int(11) not null auto_increment primary key comment 'ID',
  master_uid int(11) not null default '0' comment '店主UID',
  customer_uid int(11) not null default '0' comment '买家UID',
  createtime varchar(11) not null default '' comment '创建时间'
)engine=myisam default charset=utf8 comment='我的客户表';

drop table if exists wx_sign_config;
create table wx_sign_config(
  name varchar(32) not null default '' comment '键',
  value varchar(32) not null default '' comment '值',
  type varchar(15) not null default '' comment '类型'
)engine=myisam default charset=utf8 comment='签到配置表';

drop table if exists wx_sign_log;
create table wx_sign_log(
  id int(11) not null auto_increment primary key comment 'ID',
  uid int(11) not null default '0' comment '用户ID',
  createtime varchar(11) not null default '' comment '创建时间',
  assentnum smallint(6) not null default '0' comment '点赞数'
)engine=myisam default charset=utf8 comment='签到记录表';

drop table if exists wx_recommend_config;
create table wx_recommend_config(
  id smallint(6) not null auto_increment primary key comment 'ID',
  type tinyint(1) not null default '0' comment '推送类型 0 立即推送 1 关注 2 VIP到期前3天 3 VIP到期当天 4 VIP到期后3天 5 下单 6 VIP变动',
  title varchar(120) not null default '' comment '标题',
  content varchar(255) not null default '' comment '内容',
  link varchar(120) not null default '' comment '链接'
)engine=myisam default charset=utf8 comment='推送配置表';

drop table if exists wx_banner;
create table wx_banner(
  id smallint(6) not null auto_increment primary key comment 'ID',
  pic varchar(120) not null default '' comment '图片',
  link varchar(120) not null default '' comment '链接',
  orderby tinyint(2) not null default '0' comment '排序',
  is_show tinyint(1) not null default '0' comment '是否显示 0 未显示 1 显示',
  remark varchar(120) not null default '' comment '备注',
  createtime varchar(11) not null default '' comment '创建时间'
)engine=myisam default charset=utf8 comment='Banner表';

drop table if exists wx_feedback;
create table wx_feedback(
  id int(11) not null auto_increment primary key comment 'ID',
  uid int(11) not null default '0' comment '用户UID',
  type tinyint(1) not null default '1' comment '类型 1 意见反馈 2 投诉举报',
  remark varchar(150) not null default '' comment '备注 如是投诉则记录文章{id:title}',
  content varchar(255) not null default '' comment '内容',
  phone varchar(15) not null default '' comment '联系电话',
  createtime varchar(11) not null default '' comment '创建时间',
  is_reply tinyint(1) not null default '0' comment '是否已回复 0 未处理 1 已处理',
  reply_time varchar(11) not null default '' comment '回复时间',
  reply_content varchar(255) not null default '' comment '回复内容'
)engine=myisam default charset=utf8 comment='反馈投诉表';

drop table if exists wx_manager;
create table wx_manager(
  uid smallint(6) not null auto_increment primary key comment 'ID',
  username varchar(32) not null default '' comment '用戶名',
  password varchar(32) not null default '' comment '密碼',
  kouling varchar(15) not null default '' comment '口令',
  createtime varchar(11) not null default '' comment '创建时间',
  last_login_time varchar(11) not null default '' comment '最后登录时间',
  state tinyint(1) not null default '0' comment '管理员状态 0 正常 1 禁用'
)engine=myisam default charset=utf8 comment='管理员表';

insert into wx_manager (username,password,kouling,createtime) values ('admin', '21232f297a57a5a743894a0e4a801fc3', '123456', '1532247899');




