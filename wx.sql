-- MySQL dump 10.13  Distrib 5.5.60, for Linux (x86_64)
--
-- Host: localhost    Database: wx
-- ------------------------------------------------------
-- Server version	5.5.60-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wx_article`
--

DROP TABLE IF EXISTS `wx_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wx_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `open` tinyint(1) NOT NULL,
  `author` varchar(32) DEFAULT NULL,
  `source` varchar(100) DEFAULT NULL,
  `view` varchar(32) NOT NULL DEFAULT '1',
  `time` varchar(32) NOT NULL,
  `commend` tinyint(1) NOT NULL DEFAULT '0',
  `choice` tinyint(1) NOT NULL DEFAULT '0',
  `pic` varchar(100) DEFAULT NULL,
  `keywords` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wx_article`
--

LOCK TABLES `wx_article` WRITE;
/*!40000 ALTER TABLE `wx_article` DISABLE KEYS */;
INSERT INTO `wx_article` VALUES (1,3,'我开超市了，吃喝玩乐，小朋友的都有，进来看看吧......',1,'','','33','1530841931',1,1,'/2018-07-09/5b42e58e5beee.jpg','','我开超市了，吃喝玩乐，小朋友的都有，进来看看吧......','            &lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;1&quot; src=&quot;/Uploads/2018-07-09/5b42e15a1f53a.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;老有人问我，现在在干啥？开的什么店？\r\n&lt;/p&gt;&lt;p&gt;我想告诉你......\r\n&lt;/p&gt;&lt;p&gt;你想洗头，本店有绿叶洗发水！\r\n&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;2&quot; src=&quot;/Uploads/2018-07-09/5b42e16563ee7.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;你想洗澡，本店有绿叶沐浴露！\r\n&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;3&quot; src=&quot;/Uploads/2018-07-09/5b42e16e04aed.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;你想刷牙，本店有绿叶牙膏！\r\n&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;4&quot; src=&quot;/Uploads/2018-07-09/5b42e174ddd75.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;你皮肤受伤了，本店有绿叶修复胶！\r\n&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;5&quot; src=&quot;/Uploads/2018-07-09/5b42e17d00865.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;好友相聚，怎能少的了啤酒？我们也有哦！\r\n&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;6&quot; src=&quot;/Uploads/2018-07-09/5b42e1850910b.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;宝妈妈关心的纸尿裤，湿巾我们也有哦，把最好的爱送给宝宝！\r\n&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;7&quot; src=&quot;/Uploads/2018-07-09/5b42e192704af.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;女性每个月，总有那么几天需要用到的姨妈巾，我们也有！\r\n&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;8&quot; src=&quot;/Uploads/2018-07-09/5b42e19db3eb8.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;夏日炎炎，怎能少得了补水神器？\r\n&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;9&quot; src=&quot;/Uploads/2018-07-09/5b42e1a57a844.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;我选择绿叶的10大理由：\r\n&lt;/p&gt;&lt;p&gt;1、生物高科技企业！\r\n&lt;/p&gt;&lt;p&gt;2、民族的品牌！\r\n&lt;/p&gt;&lt;p&gt;3、健康的产业！\r\n&lt;/p&gt;&lt;p&gt;4、助人的行业！（团队合作）\r\n&lt;/p&gt;&lt;p&gt;5、管道事业！（趋势）\r\n&lt;/p&gt;&lt;p&gt;6、垄断的产品！\r\n&lt;/p&gt;&lt;p&gt;7、独特的功效！（九大功效，适应各种人群，无保质期，年年升值）\r\n&lt;/p&gt;&lt;p&gt;8、先进的制度，无级别、无囤货！\r\n&lt;/p&gt;&lt;p&gt;9、奖勤不罚懒，业绩不清零，多劳多得，无限超越！\r\n&lt;/p&gt;&lt;p&gt;10、赚钱周薪周结，做到得到！\r\n&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;'),(2,3,'如果您的日用品快用完了，真心邀请您一定要试试绿叶日用品！！吃的，',1,'','','52','1531110755',1,1,'/2018-07-09/5b42e5635528e.jpg',NULL,'如果您的日用品快用完了，真心邀请您一定要试试绿叶日用品！！吃的，喝的，洗的，抹的都有......','&lt;font size=&quot;3&quot;&gt;绿叶，仅1875元&lt;/font&gt;&lt;p&gt;解决您一家的\r\n&lt;/p&gt;&lt;p&gt;刷牙，洗头，洗手，\r\n&lt;/p&gt;&lt;p&gt;洗澡，洗碗，洗锅，洗衣，\r\n&lt;/p&gt;&lt;p&gt;并且安全，高效，绿色，环保，纯植物，\r\n&lt;/p&gt;&lt;p&gt;好而不贵，孕妇及儿童都可以用，\r\n&lt;/p&gt;&lt;p&gt;最重要的还可以提高生活品质，1875块钱能洗所有！\r\n&lt;/p&gt;&lt;p&gt;关键是我想你们都能用上放心的产品，身体才是革命的本钱。\r\n&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;1&quot; src=&quot;/Uploads/2018-07-09/5b42e44806e86.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;下面就为大家列举几个产品看一看，真正的体现物超所值的产品是什么样的！\r\n&lt;/p&gt;&lt;p&gt;&lt;font color=&quot;#ffcc00&quot;&gt;1.洋甘菊泡沫洗手液\r\n&lt;/font&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;2&quot; src=&quot;/Uploads/2018-07-09/5b42e4523cf96.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;3&quot; src=&quot;/Uploads/2018-07-09/5b42e45b98248.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;4&quot; src=&quot;/Uploads/2018-07-09/5b42e462b9999.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;纯天然植物护手 保湿滋润\r\n&lt;/p&gt;&lt;p&gt;&lt;font color=&quot;#ffcc00&quot;&gt;洋甘菊泡沫洗手液\r\n&lt;/font&gt;&lt;/p&gt;&lt;p&gt;ChamomilLA foamING hand wash\r\n&lt;/p&gt;&lt;p&gt;蕴含洋甘菊提取物，保湿成分，温和清洁，肌肤润泽\r\n&lt;/p&gt;&lt;p&gt;夏日洁净不伤手\r\n&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;5&quot; src=&quot;/Uploads/2018-07-09/5b42e46c3ba8a.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;6&quot; src=&quot;/Uploads/2018-07-09/5b42e476d27be.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;成为会员之后只需要半价！超值有木有！但是能高效的清除手上触摸到的细菌等，还我们的双手一片绿色！\r\n&lt;/p&gt;&lt;p&gt;&lt;font color=&quot;#ffcc00&quot;&gt;2.清盈滋养沐浴露\r\n&lt;/font&gt;&lt;/p&gt;&lt;p&gt;（帮助干燥、缺水的肌肤补充水分，令肌肤水润舒爽，补水保湿，展现柔滑美肌。）！\r\n&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;7&quot; src=&quot;/Uploads/2018-07-09/5b42e47e0b520.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;● 保湿效果非常的好：含有保湿成分，为肌肤补充水分，长期使用使肌肤非常的滋润、柔滑、水水嫩嫩 。\r\n&lt;/p&gt;&lt;p&gt;● 滋养效果立竿见影：含有高滋养的成分，为肌肤补充水分、油脂和营养，滋养效果看得见，皮肤不再出现皮屑！\r\n&lt;/p&gt;&lt;p&gt;● 修护能力强：使皮肤不松弛保持紧致，修护肌肤的干燥现象，使肌肤呈现年轻态。\r\n&lt;/p&gt;&lt;p&gt;远离皮屑、水水嫩嫩就选绿叶清盈滋养沐浴露！500ml的超大容量，成为会员后只需要半价元就可以得到！\r\n&lt;/p&gt;&lt;p&gt;&lt;font color=&quot;#ffcc00&quot;&gt;3.绿叶爱生活滋养柔顺洗发水\r\n&lt;/font&gt;&lt;/p&gt;&lt;p&gt;（蕴含聚谷氨酸水凝胶、维他命原、透明质酸精华，滋润头皮，滋养发芯，秀发持久轻盈顺滑，水润有光泽）\r\n&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;8&quot; src=&quot;/Uploads/2018-07-09/5b42e484a01f5.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;500g的容量，成为会员之后只需要半价，高品质的产品却是超低的价位！你值得拥有！\r\n&lt;/p&gt;&lt;p&gt;&lt;font color=&quot;#ffcc00&quot;&gt;4.绿叶海藻无氟牙膏\r\n&lt;/font&gt;&lt;/p&gt;&lt;p&gt;（关于这款牙膏的好处，我有和大家深刻的分析，真的是用过就离不开的一款爆品！）\r\n&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;9&quot; src=&quot;/Uploads/2018-07-09/5b42e48c62994.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;font color=&quot;#ffcc00&quot;&gt;5.绿叶纯天然洗衣液\r\n&lt;/font&gt;&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;10&quot; src=&quot;/Uploads/2018-07-09/5b42e496a5eef.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;这一款绿叶纯天然洗衣液，非常的方便，简直是洗衣机和懒人的标配啊，衣服泡几天都不会有异味，而是淡淡的清香，洗衣液的杀菌效果非常的好，也不会损伤衣物，洗过的东西都非常的柔软，而且不伤手。放在一个勺子里面点燃，不会出现刺鼻的气味，而是淡淡的清香味，纯绿色植物成分，安全值得信赖！\r\n&lt;/p&gt;&lt;p&gt;同样，我们的价格也非常的低，成为我们的会员之后，也可以享受半价。\r\n&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;11&quot; src=&quot;/Uploads/2018-07-09/5b42e4a039d6e.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;还有我们的绿叶护肤养肤品等，都是非常非常的实惠，质量却非常的高，真正的绿色、健康、环保产品！只需要1875元就得到那么多，所以不用再为不提高自己的生活品质找借口了！买到就是赚到，而且同时可以经营一份事业！N全齐美的事情，朋友们赶紧行动起来吧！\r\n&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;【来源\\网络  编辑：贝贝】\r\n&lt;/p&gt;&lt;p align=&quot;right&quot;&gt;【版权归原作者所有，如有侵权请联系删除】&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;12&quot; src=&quot;/Uploads/2018-07-09/5b42e4a7c2fbe.gif&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;'),(3,2,'绿叶扎10000次也不会漏气，不用补的轮胎已经研发出来了，即将量',0,'','','1','1531111617',0,0,'/2018-07-09/5b42e8c15973a.jpg',NULL,'','&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;p&gt;&lt;iframe width=&quot;640&quot; height=&quot;498&quot; src=&quot;https://v.qq.com/iframe/player.html?vid=c0533umcuh1&amp;tiny=0&amp;amp;auto=0&quot; frameborder=&quot;0&quot; allowfullscreen=&quot;&quot;&gt;&lt;/iframe&gt;&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;\r\n      &lt;p&gt;&lt;br&gt;&lt;/p&gt;');
/*!40000 ALTER TABLE `wx_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wx_banner`
--

DROP TABLE IF EXISTS `wx_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wx_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apic` varchar(100) DEFAULT NULL,
  `atitle` varchar(32) DEFAULT NULL,
  `alink` varchar(100) DEFAULT NULL,
  `bpic` varchar(100) DEFAULT NULL,
  `btitle` varchar(32) DEFAULT NULL,
  `blink` varchar(100) DEFAULT NULL,
  `cpic` varchar(100) DEFAULT NULL,
  `ctitle` varchar(32) DEFAULT NULL,
  `clink` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wx_banner`
--

LOCK TABLES `wx_banner` WRITE;
/*!40000 ALTER TABLE `wx_banner` DISABLE KEYS */;
INSERT INTO `wx_banner` VALUES (1,'/2016-12-04/584640b0d119c.jpg','科大讯飞发布会，我看到的人工智能 科大讯飞发布会，我看到的人工','../index.php?m=Home&amp;c=index&amp;a=article&amp;id=2','/2016-12-04/58464121c246c.jpg','科大讯飞发布会，我看到的人工智能','../index.php?m=Home&amp;c=index&amp;a=article&amp;id=2','/2016-12-04/5846415eb5d46.jpg','虚拟现实游戏玩太久会怎样？','../index.php?m=Home&amp;c=index&amp;a=article&amp;id=1');
/*!40000 ALTER TABLE `wx_banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wx_category`
--

DROP TABLE IF EXISTS `wx_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wx_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `pic` varchar(100) DEFAULT NULL,
  `time` varchar(32) NOT NULL,
  `keywords` varchar(32) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wx_category`
--

LOCK TABLES `wx_category` WRITE;
/*!40000 ALTER TABLE `wx_category` DISABLE KEYS */;
INSERT INTO `wx_category` VALUES (1,0,'养生调理',1,NULL,'1530784703','',''),(2,0,'事业宝典',1,NULL,'1530784745','',''),(3,0,'产品示范',1,NULL,'1531109223','','');
/*!40000 ALTER TABLE `wx_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wx_conf`
--

DROP TABLE IF EXISTS `wx_conf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wx_conf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(32) DEFAULT NULL,
  `open` tinyint(1) NOT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `qqs` varchar(32) DEFAULT NULL,
  `yuming` varchar(32) DEFAULT NULL,
  `beian` varchar(32) DEFAULT NULL,
  `title` varchar(32) DEFAULT NULL,
  `keywords` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `yid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wx_conf`
--

LOCK TABLES `wx_conf` WRITE;
/*!40000 ALTER TABLE `wx_conf` DISABLE KEYS */;
INSERT INTO `wx_conf` VALUES (1,'为美而生',1,'/2016-12-04/586b5a416b776.png','为美而生','','蜀ICP备10000000号','为美而生','为美而生','为美而生','');
/*!40000 ALTER TABLE `wx_conf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wx_goods`
--

DROP TABLE IF EXISTS `wx_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wx_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL COMMENT '分类ID',
  `title` varchar(32) NOT NULL COMMENT '商品标题',
  `open` tinyint(1) NOT NULL,
  `price` decimal(11,2) DEFAULT NULL,
  `mk_price` decimal(11,2) DEFAULT NULL,
  `view` varchar(32) NOT NULL DEFAULT '1',
  `time` varchar(32) NOT NULL,
  `commend` tinyint(1) NOT NULL DEFAULT '0',
  `choice` tinyint(1) NOT NULL DEFAULT '0',
  `pic` varchar(100) DEFAULT NULL,
  `keywords` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wx_goods`
--

LOCK TABLES `wx_goods` WRITE;
/*!40000 ALTER TABLE `wx_goods` DISABLE KEYS */;
INSERT INTO `wx_goods` VALUES (6,11,'希诺丝焕能新生塑形面膜',0,59.80,59.80,'1','1531108021',0,0,'/2018-07-09/5b42dab57efc4.jpg',NULL,NULL,'&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;1&quot; src=&quot;/Uploads/2018-07-09/5b42da628375e.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;封面图&quot; src=&quot;/Uploads/2018-07-09/5b42daa2388da.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;8&quot; src=&quot;/Uploads/2018-07-09/5b42daa221ce2.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;7&quot; src=&quot;/Uploads/2018-07-09/5b42da9aa34a8.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;6&quot; src=&quot;/Uploads/2018-07-09/5b42da8d79d8e.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;5&quot; src=&quot;/Uploads/2018-07-09/5b42da853c450.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;4&quot; src=&quot;/Uploads/2018-07-09/5b42da80c4cd4.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;3&quot; src=&quot;/Uploads/2018-07-09/5b42da7c18e4d.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;2&quot; src=&quot;/Uploads/2018-07-09/5b42da73be893.jpg&quot;&gt;&lt;/p&gt;\r\n      &lt;p&gt;&lt;br&gt;&lt;/p&gt;'),(7,12,'30ml卡丽施清爽抑菌喷剂',0,17.80,17.80,'1','1531108794',0,0,'/2018-07-09/5b42ddba2b5f2.jpg',NULL,NULL,'&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;1&quot; src=&quot;/Uploads/2018-07-09/5b42dc733e7c9.png&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;2&quot; src=&quot;/Uploads/2018-07-09/5b42dcbe6129a.png&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;5&quot; src=&quot;/Uploads/2018-07-09/5b42dce7dcaee.png&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;2&quot; src=&quot;/Uploads/2018-07-09/5b42dc72e4ed3.png&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;4&quot; src=&quot;/Uploads/2018-07-09/5b42dcd82583a.png&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;3&quot; src=&quot;/Uploads/2018-07-09/5b42dccfe65c6.png&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;6&quot; src=&quot;/Uploads/2018-07-09/5b42dd04d6ad4.png&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;7&quot; src=&quot;/Uploads/2018-07-09/5b42dd24c3a8d.png&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;9&quot; src=&quot;/Uploads/2018-07-09/5b42ddb047e96.png&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;img style=&quot;max-width: 100%;&quot; alt=&quot;11&quot; src=&quot;/Uploads/2018-07-09/5b42dd7ddab5e.jpg&quot;&gt;&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;');
/*!40000 ALTER TABLE `wx_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wx_goods_class`
--

DROP TABLE IF EXISTS `wx_goods_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wx_goods_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `pic` varchar(100) DEFAULT NULL,
  `time` varchar(32) NOT NULL,
  `keywords` varchar(32) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wx_goods_class`
--

LOCK TABLES `wx_goods_class` WRITE;
/*!40000 ALTER TABLE `wx_goods_class` DISABLE KEYS */;
INSERT INTO `wx_goods_class` VALUES (9,0,' 家清厨卫',2,NULL,'1530790430','',''),(10,0,' 食品酒饮',2,NULL,'1530790443','',''),(11,0,'个护美妆',1,NULL,'1531106595','',''),(12,0,' 家纺家居',1,NULL,'1531106696','',''),(13,0,'母婴玩具',1,NULL,'1531106706','',''),(14,0,' 服饰配饰',1,NULL,'1531106714','',''),(15,0,' 鞋靴配饰',1,NULL,'1531106722','',''),(16,0,'办公文具',1,NULL,'1531106736','','');
/*!40000 ALTER TABLE `wx_goods_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wx_links`
--

DROP TABLE IF EXISTS `wx_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wx_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `link` varchar(100) NOT NULL,
  `time` varchar(32) NOT NULL,
  `pic` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wx_links`
--

LOCK TABLES `wx_links` WRITE;
/*!40000 ALTER TABLE `wx_links` DISABLE KEYS */;
INSERT INTO `wx_links` VALUES (1,'百度一下','https://www.baidu.com/','1480767669','/2016-12-04/584388b25b051.png'),(2,'360搜索','https://www.so.com/','1480767682','/2016-12-04/5843891d1e739.png');
/*!40000 ALTER TABLE `wx_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wx_member`
--

DROP TABLE IF EXISTS `wx_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wx_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `kouling` varchar(32) NOT NULL DEFAULT '',
  `openid` varchar(32) NOT NULL DEFAULT '' COMMENT '微信openid',
  `createtime` varchar(15) NOT NULL DEFAULT '' COMMENT '注册时间',
  `qrimg` varchar(255) NOT NULL DEFAULT '' COMMENT '微信二维码',
  `proverb` varchar(150) NOT NULL DEFAULT '' COMMENT '格言',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wx_member`
--

LOCK TABLES `wx_member` WRITE;
/*!40000 ALTER TABLE `wx_member` DISABLE KEYS */;
INSERT INTO `wx_member` VALUES (1,'admin','21232f297a57a5a743894a0e4a801fc3','123456','','','','','');
/*!40000 ALTER TABLE `wx_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wx_navtop`
--

DROP TABLE IF EXISTS `wx_navtop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wx_navtop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `link` varchar(100) DEFAULT NULL,
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `time` varchar(32) NOT NULL,
  `blank` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wx_navtop`
--

LOCK TABLES `wx_navtop` WRITE;
/*!40000 ALTER TABLE `wx_navtop` DISABLE KEYS */;
INSERT INTO `wx_navtop` VALUES (2,0,'../index.php',1,1,'网站首页','1480822884',0),(3,6,'../index.php?m=Home&amp;c=index&amp;a=category&amp;id=5',1,2,'虚拟现实','1480822942',0),(4,6,'../index.php?m=Home&amp;c=index&amp;a=category&amp;id=6',1,3,'人工智能','1480823178',0),(5,0,'http://www.tpt360.com/',1,4,'官方网站','1480823455',1),(6,0,'',1,3,'科技头条','1486171934',0),(7,0,'',1,2,'新闻资讯','1486171999',0),(8,7,'../index/2.html',1,1,'国内新闻','1486172025',0),(9,7,'../index/3.html',1,2,'国际新闻','1486172045',0);
/*!40000 ALTER TABLE `wx_navtop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wx_post`
--

DROP TABLE IF EXISTS `wx_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wx_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `open` tinyint(1) NOT NULL,
  `author` varchar(32) DEFAULT NULL,
  `source` varchar(100) DEFAULT NULL,
  `view` varchar(32) NOT NULL DEFAULT '1',
  `time` varchar(32) NOT NULL,
  `commend` tinyint(1) NOT NULL DEFAULT '0',
  `choice` tinyint(1) NOT NULL DEFAULT '0',
  `pic` varchar(100) DEFAULT NULL,
  `keywords` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wx_post`
--

LOCK TABLES `wx_post` WRITE;
/*!40000 ALTER TABLE `wx_post` DISABLE KEYS */;
INSERT INTO `wx_post` VALUES (4,10,'芦荟胶',0,'芦荟胶',NULL,'1','1531106378',0,0,'/2018-07-09/5b42d44a680cb.jpg',NULL,NULL,''),(5,10,'牙膏',0,'牙膏',NULL,'1','1531106437',0,0,'/2018-07-09/5b42d48536a1a.jpg',NULL,NULL,''),(6,10,'补水',0,'补水',NULL,'1','1531106460',0,0,'/2018-07-09/5b42d49cd2996.jpg',NULL,NULL,'');
/*!40000 ALTER TABLE `wx_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wx_post_class`
--

DROP TABLE IF EXISTS `wx_post_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wx_post_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `pic` varchar(100) DEFAULT NULL,
  `time` varchar(32) NOT NULL,
  `keywords` varchar(32) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wx_post_class`
--

LOCK TABLES `wx_post_class` WRITE;
/*!40000 ALTER TABLE `wx_post_class` DISABLE KEYS */;
INSERT INTO `wx_post_class` VALUES (8,0,'展业',1,NULL,'1530790362','',''),(9,0,'增员',1,NULL,'1530790377','',''),(10,0,'产品',1,NULL,'1531106277','','');
/*!40000 ALTER TABLE `wx_post_class` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-19  1:27:14
