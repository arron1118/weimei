function getQueryString(name){
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r!=null) return r[2]; return '';
}
$(function(){
    var str = getQueryString('cur');
    var cur = '', cur2 = '', cur3 = '', cur4 = '';
    if(!str){
         cur="on";
    }else if(str==2){
         cur2="on";
    }else if(str==3){
         cur3="on";
    }else if(str==4){
         cur4="on";
    }
    var html = '<ul class="footnav box-flex">\
            <li class="'+cur+'">\
                <a href="/index.php?m=Home&c=index&a=index">\
                    <span class="glyphicon glyphicon-home"></span>\
                    <span class="full-block">微站</span>\
                </a>\
            </li>\
            <li class="'+cur2+'">\
                <a href="/index.php?m=Home&c=index&a=goods&cur=2">\
                    <span class="glyphicon glyphicon-list-alt"></span>\
                    <span class="full-block">产品</span>\
                </a>\
            </li>\
            <li class="'+cur3+'">\
                <a href="/index.php?m=Home&c=User&a=index&cur=3">\
                    <span class="glyphicon glyphicon-user"></span>\
                    <span class="full-block">我的</span>\
                </a>\
            </li>\
            <li class="'+cur4+'">\
                <a href="/index.php?m=Home&c=index&a=post&cur=4">\
                    <span class="glyphicon glyphicon-picture"></span>\
                    <span class="full-block">海报</span>\
                </a>\
            </li>\
        </ul>';
    $('#footer').html(html)
})