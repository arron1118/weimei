<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>我的客户</title>

    <!-- Bootstrap -->
    <!-- <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="/Templet/default/Public/css/bootstrap.min.css" rel="stylesheet">
    <link href="/Templet/default/Public/css/style.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo U('Home/User/index');?>">
                <span class="glyphicon glyphicon-menu-left"></span>
            </a>
            <div class="nav-title">我的客户</div>
        </div>
    </nav>
    <div class="height50"></div>

    <div class="tips">
        暂无任何资料
    </div>

    <div class="copy-right">
        <p>Copyright©2018 版权所有 </p>
    </div>

    <footer class="footer navbar-fixed-bottom ">
        <ul class="footnav box-flex">
            <li class="on">
                <a href="#">
                    <span class="glyphicon glyphicon-home"></span>
                    <span class="full-block">我的微站</span>
                </a>
            </li>
            <li>
                <a href="<?php echo U('Home/Index/goods', ['cur' => 2]);?>">
                    <span class="glyphicon glyphicon-list-alt"></span>
                    <span class="full-block">产品展示</span>
                </a>
            </li>
            <!-- <li>
                    <a href="13chart.html">
                        <span class="glyphicon glyphicon-comment"></span>
                        <span class="full-block open">免费咨询</span>
                    </a>
                </li> -->
            <li>
                <a href="<?php echo U('Home/User/index', ['cur' => 4]);?>">
                    <span class="glyphicon glyphicon-user"></span>
                    <span class="full-block">个人中心</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="glyphicon glyphicon-picture"></span>
                    <span class="full-block">更多服务</span>
                </a>
            </li>
        </ul>
    </footer>

    <!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
    <script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
    <!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>

</html>