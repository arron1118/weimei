<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>3文章详情</title>

    <!-- Bootstrap -->
    <!-- <link href="https://cdn.boot/Templet/default/Public/css.com/bootstrap/3.3.7//Templet/default/Public/css/bootstrap.min./Templet/default/Public/css" rel="stylesheet"> -->
    <link href="/Templet/default/Public/css/bootstrap.min.css" rel="stylesheet">
    <link href="/Templet/default/Public/css/style.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo U('Home/index/index');?>">
                <span class="glyphicon glyphicon-menu-left"></span>返回
            </a>
        </div>
    </nav>
    <div class="height50"></div>
    <div class="container">
        <div class="successDetail">
            <p><?php echo ($tptl["title"]); ?></p>
        </div>
        <div class="successDetail_time">
            <span><?php echo (date('Y-m-d H:i:s',$tptl["time"])); ?></span>
            点击量<span class="blue"><?php echo ($tptl["view"]); ?></span>
            <a href="#" class="btn btn-success user-btn pull-right">换成我的</a>
            <div class="clearfix"></div>
        </div>
<!--        <div class="index-user fumargin news-user">
            <div class="user-pic">
                <img src="/Templet/default/Public/images/user-pic.jpg">
            </div>
            <div class="user-right">
                <div class="slider-left">
                    <p>
                        <span class="glyphicon glyphicon-earphone"></span>18319556889
                        <span class="tel">打电话</span>
                    </p>
                    <p>
                        <span class="glyphicon glyphicon-comment"></span>18319556889
                        <span class="addweixin">加微信</span>
                    </p>
                </div>
                <div class="slider-right">
                    <a href="user.html" class="btn btn-warning user-btn">在线咨询</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="new-banner">
            <img src="/Templet/default/Public/images/new-banner.gif">
        </div>-->

        <div class="successDetail_nr">
            <?php echo ($content); ?>
        </div>

<!--        <div class="new-banner">
            <img src="/Templet/default/Public/images/new-banner2.gif">
        </div>

        <div class="mingpian">
            <div class="col-xs-6">
                <div class="user-pic text-center">
                    <img src="/Templet/default/Public/images/user-pic.jpg">
                    <div class="user-name">老郭</div>
                </div>
                <div class="clearfix"></div>

                <span class="user-tel text-center">
                    <span class="glyphicon glyphicon-earphone"></span>
                    <span>18319556889</span>
                </span>

                <div class="user-addr">
                    <p>职称：</p>
                    <p>地区：</p>
                </div>
            </div>

            <div class="col-xs-6 news-ewm">
                <p class="add-me">加我微信</p>
                <p>免费咨询</p>
                <img src="/Templet/default/Public/images/erweima.gif">
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="wanshan text-center">
            <a href="myinfo.html" class="btn btn-danger full-width">完善我的名片>></a>
        </div>

        <div class="successDetail_bottom">
            <a href="HH_details.html">
                <span>下一篇</span>
            </a>EB-1A美国杰出人才移民最快获批记录美国杰出人才移民最快获批记录-2天！</div>


        <div class="usa_eba">
            <span class="glyphicon glyphicon-thumbs-up"></span>
            <span>相关推荐</span>
        </div>
        <div class="successDetail_tj">
            <a href="#">
                <li>
                    <span>EB-1A美国杰出人才移民最快获批记录-2天！</span>
                </li>
            </a>
            <a href="#">
                <li>
                    <span>美国移民局政策缩紧，面谈范围再扩大！</span>
                </li>
            </a>
            <a href="#">
                <li>
                    <span>美国王欣律师行专治移民局各种“不服”</span>
                </li>
            </a>
        </div>


    </div>-->


<!--    <div class="copy-right">
        <p>Copyright©2018 版权所有 </p>
    </div>-->

    <!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
    <script src="https://cdn.boot/Templet/default/Public/css.com/jquery/1.12.4/jquery.min.js"></script>
    <!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
    <script src="https://cdn.boot/Templet/default/Public/css.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>

</html>