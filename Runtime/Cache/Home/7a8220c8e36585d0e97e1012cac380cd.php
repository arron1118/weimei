<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>4-1产品详情</title>

    <!-- Bootstrap -->
    <!-- <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="/Templet/default/Public/css/bootstrap.min.css" rel="stylesheet">
    <link href="/Templet/default/Public/css/style.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo U('Home/index/goods');?>">
                <span class="glyphicon glyphicon-menu-left"></span>
            </a>
            <div class="nav-title">素养食品</div>
        </div>
    </nav>
    <div class="height50"></div>

    <div class="product-detail">
        <img src="/Uploads<?php echo ($goods["pic"]); ?>">
        <div class="detail-content">
            <h4><?php echo ($goods["title"]); ?></h4>
            <span class="pro-name"><?php echo ($goods["price"]); ?></span>
            <p>规格：160g</p>
            <div class="pro-line">
                
                <?php echo ($goods['description']); ?>
                </div>
   
                <?php echo ($content); ?>
    <div class="line"></div>

    <div class="other-pro">
        <span class="glyphicon glyphicon-thumbs-up"></span>
        <span>相关产品</span>
    </div>
    <div class="other-pro-list">
        <?php if(is_array($goods_list)): $i = 0; $__LIST__ = $goods_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo U('Home/index/goods_detail',array('id'=>$vo['id']));?>">
            <div class="media">
                <div class="media-left">
                    <img class="media-object" src="/Uploads<?php echo ($vo["pic"]); ?>">
                </div>
                <div class="media-body">
                    <h5 class="media-heading"><?php echo ($vo["title"]); ?></h5>
                    ￥<?php echo ($vo["price"]); ?>
                </div>
            </div>
        </a><?php endforeach; endif; else: echo "" ;endif; ?>
    </div>

    <div class="line"></div>

    <div class="copy-right">
        <p>Copyright©2018 版权所有 </p>
    </div>

    <footer class="footer navbar-fixed-bottom footer-detail">
        <ul class="footnav box-flex">
            <li>
                <a href="Index_index.html">
                    <span class="glyphicon glyphicon-home"></span>
                    <span class="full-block">我的网站</span>
                </a>
            </li>
            <li>
                <a  class="btn btn-warning">
                    电话购买
                </a>
            </li>
            <li>
                <a href="cart.html" class="btn btn-danger">
                    立即预定
                </a>
            </li>
        </ul>
    </footer>

    <!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
    <script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
    <!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>

</html>