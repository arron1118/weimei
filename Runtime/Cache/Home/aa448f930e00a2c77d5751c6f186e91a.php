<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>index</title>

    <!-- Bootstrap -->
    <!--<link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="/Templet/default/Public/css/bootstrap.min.css" rel="stylesheet">
    <link href="/Templet/default/Public/css/style.css" rel="stylesheet">
</head>

<body>
<div class="index-user">
    <div class="user-pic">
        <img src="/Templet/default/Public/images/user-pic.jpg">
    </div>
    <div class="user-right">
        <div class="slider-left">
            <span>神九</span>18319556889
        </div>
        <div class="slider-right">
            <a href="<?php echo U('Home/User/index', ['cur' => 4]);?>" class="btn btn-success user-btn">进入</a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <!-- <li data-target="#carousel-example-generic" data-slide-to="2"></li> -->
    </ol>
    <div class="carousel-inner" role="listbox" id='add'>
        <?php if(is_array($tptg)): $i = 0; $__LIST__ = $tptg;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="item  {if $vo['first'] eq 1 }active{/if}">
                <a href="<?php echo U('Home/index/article',array('id'=>$vo['id']));?>"><img  style="height:194px; width:414px;" src="/Uploads<?php echo ($vo["pic"]); ?>"></a>
            </div><?php endforeach; endif; else: echo "" ;endif; ?>

        <!--            <div class="item">
                        <img src="/Templet/default/Public/images/2.gif">
                    </div>-->
    </div>

</div>

<div class="news-class-box">
    <ul class="news-class-list">
        <?php if(is_array($category)): $i = 0; $__LIST__ = $category;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li class="active">
                <a href="newsList.html"><?php echo ($vo['name']); ?></a>
            </li><?php endforeach; endif; else: echo "" ;endif; ?>
        <div class="clearfix"></div>
    </ul>
</div>

<div class="line"></div>

<div class="search">
    <form class="form-inline">
        <div class="form-group">
            <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
            <div class="input-group">
                <form action="/index.php/search.html" method="get">
                    <input type="text" class="form-control" id="exampleInputAmount" value="<?php echo I('kw');?>" placeholder="输入关键字，找文章">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-search"></span><button type="submit">搜索</button></div>
                </form>
            </div>

        </div>
    </form>
</div>

<div class="news-list">
    <?php if(is_array($tptg)): $i = 0; $__LIST__ = $tptg;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo1): $mod = ($i % 2 );++$i;?><div class="new-dt">
            <a href="<?php echo U('Home/index/article',array('id'=>$vo1['id']));?>">
                <h4><?php echo ($vo1['title']); ?></h4>
            </a>
            <a href="<?php echo U('Home/index/article',array('id'=>$vo1['id']));?>">
                <img src="/Uploads<?php echo ($vo1['pic']); ?>">
            </a>
            <span><?php echo (date('Y-m-d H:i:s',$vo1["time"])); ?></span>
            <!--            <span>浏览935</span>-->
        </div><?php endforeach; endif; else: echo "" ;endif; ?>
</div>

<div class="search">
    <form class="form-inline">
        <div class="form-group">
            <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
            <div class="input-group">
                <input type="text" class="form-control" id="exampleInputAmount" placeholder="输入关键字，找文章">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-search"></span>搜索</div>
            </div>
        </div>
    </form>
</div>

<!-- <div class="page-change">
    <div class="btn-group" role="group" aria-label="...">
        <button type="button" class="btn btn-default">
            <span class="glyphicon glyphicon-chevron-left"></span>上一页</button>
        <button type="button" class="btn btn-default">
            <span class="glyphicon glyphicon-chevron-right"></span>下一页</button>
    </div>
</div> -->

<!--  <div class="copy-right">
     <p>Copyright©2018 版权所有 </p>
 </div> -->

<footer class="footer navbar-fixed-bottom" id="footer">

</footer>

<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="/Templet/default/Public/js/footer.js"></script>
</body>

</html>