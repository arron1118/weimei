<?php
namespace Admin\Controller;

use Think\Controller;
class GoodsclassController extends CommonController
{
    public function index()
    {
        $goods_class = D('goods_class');
        $c = $goods_class->where("tid = 0")->order('id ASC')->select();
        $cs = $goods_class->where("tid != 0")->order('id ASC')->select();
        $this->assign("c", $c);
        $this->assign("cs", $cs);
        $this->display();
    }
    public function add()
    {
        $goods_class = D('goods_class');
        $c = $goods_class->where("tid=0")->select();
        $this->assign("c", $c);
        $this->display();
    }
    public function doadd()
    {
        $goods_class = D('goods_class');
        $data = $goods_class->create();
        $data['time'] = time();
        if ($_FILES['pic']['tmp_name'] != '') {
            $upload = new \Think\Upload();
            $upload->maxSize = 3145728;
            $upload->exts = array('jpg', 'gif', 'png', 'jpeg');
            $upload->rootPath = './Uploads/';
            $upload->savePath = '/';
            $info = $upload->uploadOne($_FILES['pic']);
            if (!$info) {
                $this->error($upload->getError());
            } else {
                $data['pic'] = $info['savepath'] . $info['savename'];
            }
        }
        $result = $goods_class->add($data);
        if ($result > 0) {
            $this->success('添加成功！', U('index'));
        } else {
            $this->error('添加失败！');
        }
    }
    public function edit($id)
    {
        $goods_class = D('goods_class');
        $c = $goods_class->find($id);
        $this->assign('c', $c);
        $cs = $goods_class->where("tid = 0")->select();
        $this->assign('cs', $cs);
        $this->display();
    }
    public function doedit()
    {
        $goods_class = D('goods_class');
        $data = $goods_class->create();
        if ($_FILES['pic']['tmp_name'] != '') {
            $upload = new \Think\Upload();
            $upload->maxSize = 3145728;
            $upload->exts = array('jpg', 'gif', 'png', 'jpeg');
            $upload->rootPath = './Uploads/';
            $upload->savePath = '/';
            $info = $upload->uploadOne($_FILES['pic']);
            if (!$info) {
                $this->error($upload->getError());
            } else {
                $data['pic'] = $info['savepath'] . $info['savename'];
            }
        }
        $result = $goods_class->save($data);
        if ($result > 0) {
            $this->success('修改成功！', U('index'));
        } else {
            $this->error('修改失败！');
        }
    }
    public function delete($id)
    {
        $goods_class = D("goods_class");
        $check = $goods_class->where("tid={$id}")->find();
        if ($check != null) {
            $this->error("请先删除子栏目");
        } else {
            $result = $goods_class->delete($id);
        }
        if ($result > 0) {
            $this->success("删除成功！");
        } else {
            $this->error("删除失败！");
        }
    }
}