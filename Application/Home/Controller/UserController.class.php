<?php
/**
 * Created by PhpStorm.
 * User: qinqingxun
 * Date: 2018/7/16
 * Time: 23:08
 * 用户中心
 */

namespace Home\Controller;

use Think\Controller;

class UserController extends BaseController
{


    public function index()
    {
        $this->display();
    }

    public function myinfo()
    {
        $this->display();
    }

    public function mycustomer()
    {
        $this->display();
    }

    public function myorder()
    {
        $this->display();
    }

    public function picturelist()
    {
        $this->display();
    }

    public function orderdetail()
    {
        $this->display();
    }

    public function openvip()
    {
        $this->display();
    }
}
