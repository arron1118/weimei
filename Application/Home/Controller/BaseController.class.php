<?php
namespace Home\Controller;

use Think\Controller;
class BaseController extends Controller
{
    private $appid = 'wx01dd043e3037b2e5';
    private $appsecret = '89008d4cf8e171c71b7f093896c56643';
    protected $login = false;
    protected $memberInfo = null;

    public function __construct()
    {
        parent::__construct();
        $navtop = D('navtop');
        $tpta = $navtop->where("tid = 0")->order('sort ASC')->select();
        $tptas = $navtop->where("tid != 0")->order('sort ASC')->select();
        $this->assign("tpta", $tpta);
        $this->assign("tptas", $tptas);
        $article = D('article');
        $commend['commend'] = 1;
        $choice['choice'] = 1;
        $open['open'] = 1;
        $tptb = $article->limit(3)->order('id DESC')->where($commend)->where($open)->select();
        $this->assign('tptb', $tptb);
        $tptc = $article->limit(5)->order('id DESC')->where($choice)->where($open)->select();
        $this->assign('tptc', $tptc);
        $links = D('links');
        $tptd = $links->select();
        $this->assign('tptd', $tptd);
        $conf = D('conf');
        $tpte = $conf->find(1);
        $this->assign("tpte", $tpte);
        $tags = C('WEB_TAG');
        $tagss = explode(',', $tags);
        $this->assign('tagss', $tagss);

        if (!$this->login) {
            //$this->getUserInfo();
        }

    }


    /**
     * 设置网络请求配置
     * @param $curl
     * @param bool $https
     * @param string $method
     * @param null $data
     * @return mixed|string
     */
    public function _request($curl,$https=true,$method='GET',$data=null)
    {
        // 创建一个新cURL资源
        $ch = curl_init();

        // 设置URL和相应的选项
        curl_setopt($ch, CURLOPT_URL, $curl);    //要访问的网站
        curl_setopt($ch, CURLOPT_HEADER, false);    //启用时会将头文件的信息作为数据流输出。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  //将curl_exec()获取的信息以字符串返回，而不是直接输出。

        if($https){
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  //FALSE 禁止 cURL 验证对等证书（peer's certificate）。
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  //验证主机
        }
        if($method == 'POST'){
            curl_setopt($ch, CURLOPT_POST, true);  //发送 POST 请求
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  //全部数据使用HTTP协议中的 "POST" 操作来发送。
        }


        // 抓取URL并把它传递给浏览器
        $content = curl_exec($ch);
        if ($content  === false) {
            return "网络请求出错: " . curl_error($ch);
            exit();
        }
        //关闭cURL资源，并且释放系统资源
        curl_close($ch);

        return $content;
    }

    /**
     * 获取用户的openid
     * @param string $redirect_url
     * @return mixed
     */
    public function baseAuth($redirect_url = '')
    {
        $redirect_url = 'http://m.banjiquan.com.cn/index.php';
        //1.准备scope为snsapi_base网页授权页面
        $baseurl = urlencode($redirect_url);
        $snsapi_base_url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$this->appid.'&redirect_uri='.$baseurl.'&response_type=code&scope=snsapi_base&state=YQJ#wechat_redirect';

        //2.静默授权,获取code
        //页面跳转至redirect_uri/?code=CODE&state=STATE
        $code = $_GET['code'];
        if( !isset($code) ){
            header('Location:'.$snsapi_base_url);
        }

        //3.通过code换取网页授权access_token和openid
        $curl = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$this->appid.'&secret='.$this->appsecret.'&code='.$code.'&grant_type=authorization_code';
        $content = $this->_request($curl);
        $result = json_decode($content,true);


        return $result['openid'];
    }

    /**
     * 获取微信access_token
     * @return mixed
     */
    private function getAccessToken()
    {
        $curl = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $this->appid . '&secret=' . $this->appsecret;
        $result = $this->_request($curl);
        $result = json_decode($result, true);

        return $result['access_token'];
    }

    /**
     * 获取微信userInfo
     */
    private function getUserInfo()
    {
        $accessToken = $this->getAccessToken();
        $redirect_url = 'http://m.banjiquan.com.cn/api/validate.php';
        $openid = $this->baseAuth($redirect_url);
        $curl = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token=' . $accessToken . '&openid=' . $openid . '&lang=zh_CN';
        $result = $this->_request($curl);
        $result = json_decode($result, true);

        if ($result['openid']) {
            $m = D('Member');
            $memberInfo = $m->where(['openid' => $result['openid']])->find;
            if ($memberInfo) {
                $this->memberInfo = $memberInfo;
            } else {
                $data = [
                    'username' => $result['nickname'],
                    'openid' => $result['openid'],
                    'avatar' => $result['headimgurl'],
                    'createtime' => time(),
                ];

                $uid = $m->data($data)->add();
                var_dump($data);
                var_dump($uid);
                if ($uid) {
                    $this->memberInfo = $m->find($uid);
                    $this->login = true;
                    var_dump($this->memberInfo);
                }
            }
        }
    }
}